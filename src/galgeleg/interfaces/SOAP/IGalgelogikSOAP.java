package galgeleg.interfaces.SOAP;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.rmi.RemoteException;
import java.util.ArrayList;

@WebService

public interface IGalgelogikSOAP {
    @WebMethod
    ArrayList<String> getBrugteBogstaver();

    @WebMethod
    String getSynligtOrd();

    @WebMethod
    String getOrdet();

    @WebMethod
    int getAntalForkerteBogstaver();

    @WebMethod
    boolean erSidsteBogstavKorrekt();

    @WebMethod
    boolean erSpilletVundet();

    @WebMethod
    boolean erSpilletTabt();

    @WebMethod
    boolean erSpilletSlut();

    @WebMethod
    void nulstil();

    @WebMethod
    void gætBogstav(String bogstav);

    @WebMethod
    void logStatus();

    @WebMethod
    String getStatusBesked() throws RemoteException;

    @WebMethod
    void hentOrdFraDr() throws Exception;
}
