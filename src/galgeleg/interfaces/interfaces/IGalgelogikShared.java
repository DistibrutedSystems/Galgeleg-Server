/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgeleg.interfaces.interfaces;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Tobias
 */
public interface IGalgelogikShared extends Serializable {
    ArrayList<String> getBrugteBogstaver();

    String getSynligtOrd();

    String getOrdet();

    int getAntalForkerteBogstaver();

    boolean erSidsteBogstavKorrekt();

    boolean erSpilletVundet();

    boolean erSpilletTabt();

    boolean erSpilletSlut();

    void nulstil();

    void gætBogstav(String bogstav);

    void logStatus();

    void hentOrdFraDr() throws Exception;

    String getStatusBesked();


}
