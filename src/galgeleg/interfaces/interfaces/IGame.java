package galgeleg.interfaces.interfaces;

import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.util.HashMap;
import java.util.Map;

public interface IGame extends java.rmi.Remote {
    HashMap<String, IGalgelogikShared> get() throws Exception;

    void put(Map<String, String> login) throws Exception;
}