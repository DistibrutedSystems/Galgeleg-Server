package galgeleg.interfaces.interfaces;

import java.util.List;

public interface IHighscore extends java.rmi.Remote {
    List<String> get() throws java.rmi.RemoteException;
    //public boolean submitScore(String score, String username)throws java.RMI.RemoteException;
}
