package galgeleg.interfaces.interfaces;



import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.util.Map;

public interface IGameID extends java.rmi.Remote {
    IGalgelogikShared get(String id) throws java.rmi.RemoteException;

    void put(String id, IGalgelogikShared game, Map<String, String> login) throws java.rmi.RemoteException;

    void delete(String id, Map<String, String> login) throws java.rmi.RemoteException;
}
