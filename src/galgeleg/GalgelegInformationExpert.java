/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgeleg;

import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author Tobias
 */
public class GalgelegInformationExpert {
    private HashMap<String, IGalgelogikRMI> spilListe;
    private List<String> scoreList;

    private static GalgelegInformationExpert gie;

    public static GalgelegInformationExpert getInstance() {
        if (gie == null) {
            gie = new GalgelegInformationExpert();
        }
        return gie;
    }

    private GalgelegInformationExpert() {
        spilListe = new HashMap<>();
        scoreList = new ArrayList<>();
    }

    public HashMap<String, IGalgelogikRMI> getSpilListe() {
        return spilListe;
    }

    public void setSpilListe(HashMap<String, IGalgelogikRMI> spilListe) {
        //System.out.println(spilListe.get("165205"));
        this.spilListe = spilListe;
    }

    public List<String> getScoreList() {
        return scoreList;
    }

    public void addScore(String score) {
        scoreList.add(score); //Example
    }


}
