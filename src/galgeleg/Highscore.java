package galgeleg;

import galgeleg.interfaces.RMI.IHighscore;

import java.rmi.server.UnicastRemoteObject;
import java.util.List;

public class Highscore extends UnicastRemoteObject implements IHighscore {

    public Highscore() throws java.rmi.RemoteException, Exception {
        /*
       scoreList = new ArrayList<>();
       String[] empty = {"0","n/a"};

        for (int i = 0; i < 5; i++) {
            scoreList.add(new ArrayList<String>(Arrays.asList("0", "n/a")));
        }*/
    }

    public List<String> get() {
        return GalgelegInformationExpert.getInstance().getScoreList();
    }

    /*
    @Override
    public boolean submitScore(String newScore, String username) {
        for(ArrayList<String> score : scoreList){
            if(Integer.parseInt(newScore) > Integer.parseInt(score.get(0))){
                scoreList.add(new ArrayList<String> (Arrays.asList(newScore, username)));
                Collections.sort(scoreList, new Comparator<ArrayList<String>>() {
                    @Override
                    public int compare(ArrayList<String> o1, ArrayList<String> o2) {
                        return o2.get(0).compareTo(o1.get(0));
                    }
                });
                return true;
            }
        }

        return false;
    }*/
}
