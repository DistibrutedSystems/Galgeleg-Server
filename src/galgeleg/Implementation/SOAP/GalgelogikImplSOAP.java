package galgeleg.Implementation.SOAP;

import galgeleg.Implementation.GalgelogikShared;
import galgeleg.interfaces.SOAP.IGalgelogikSOAP;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import javax.jws.WebService;
import java.rmi.RemoteException;
import java.util.ArrayList;

@WebService(endpointInterface = "galgeleg.Implementation.SOAP.GalgelogikImplSOAP")
public class GalgelogikImplSOAP implements IGalgelogikSOAP {

    public IGalgelogikShared galgelogikShared;

    public GalgelogikImplSOAP() throws Exception{
        galgelogikShared = new GalgelogikShared();
    }

    public GalgelogikImplSOAP(IGalgelogikShared galgelogikShared) {
        this.galgelogikShared = galgelogikShared;
    }

    @Override
    public ArrayList<String> getBrugteBogstaver() {
        return galgelogikShared.getBrugteBogstaver();
    }

    @Override
    public String getSynligtOrd() {
        return galgelogikShared.getSynligtOrd();
    }

    @Override
    public String getOrdet() {
        return galgelogikShared.getOrdet();
    }

    @Override
    public int getAntalForkerteBogstaver() {
        return galgelogikShared.getAntalForkerteBogstaver();
    }

    @Override
    public boolean erSidsteBogstavKorrekt() {
        return galgelogikShared.erSidsteBogstavKorrekt();
    }

    @Override
    public boolean erSpilletVundet() {
        return galgelogikShared.erSpilletVundet();
    }

    @Override
    public boolean erSpilletTabt() {
        return galgelogikShared.erSpilletTabt();
    }

    @Override
    public boolean erSpilletSlut() {
        return galgelogikShared.erSpilletSlut();
    }

    @Override
    public void nulstil() {
        galgelogikShared.nulstil();
    }

    @Override
    public void gætBogstav(String bogstav) {
        galgelogikShared.gætBogstav(bogstav);
    }

    @Override
    public void logStatus() {
        galgelogikShared.logStatus();
    }

    @Override
    public void hentOrdFraDr() throws Exception {
        galgelogikShared.hentOrdFraDr();
    }

    @Override
    public String getStatusBesked() throws RemoteException {
        return galgelogikShared.getStatusBesked();
    }
}
