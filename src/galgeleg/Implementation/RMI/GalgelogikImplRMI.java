package galgeleg.Implementation.RMI;

import galgeleg.Implementation.GalgelogikShared;
import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class GalgelogikImplRMI extends UnicastRemoteObject implements IGalgelogikRMI {

    public IGalgelogikShared galgelogikShared;


    public GalgelogikImplRMI() throws Exception{
        this.galgelogikShared = new GalgelogikShared();
    }

    public GalgelogikImplRMI(IGalgelogikShared galgelogikShared) throws RemoteException {
        this.galgelogikShared = galgelogikShared;
    }

    public ArrayList<String> getBrugteBogstaver() throws java.rmi.RemoteException {
        return galgelogikShared.getBrugteBogstaver();
    }

    public String getSynligtOrd() throws java.rmi.RemoteException {
        return galgelogikShared.getSynligtOrd();
    }

    public String getOrdet() throws java.rmi.RemoteException {
        return galgelogikShared.getOrdet();
    }

    public int getAntalForkerteBogstaver() throws java.rmi.RemoteException {
        return galgelogikShared.getAntalForkerteBogstaver();
    }

    public boolean erSidsteBogstavKorrekt() throws java.rmi.RemoteException {
        return galgelogikShared.erSidsteBogstavKorrekt();
    }

    public boolean erSpilletVundet() throws java.rmi.RemoteException {
        return galgelogikShared.erSpilletVundet();
    }

    public boolean erSpilletTabt() throws java.rmi.RemoteException {
        return galgelogikShared.erSpilletTabt();
    }

    public boolean erSpilletSlut() throws java.rmi.RemoteException {
        return galgelogikShared.erSpilletSlut();
    }

    public void nulstil() throws java.rmi.RemoteException {
        galgelogikShared.nulstil();
    }

    public void gætBogstav(String bogstav) throws java.rmi.RemoteException {
        galgelogikShared.gætBogstav(bogstav);
    }

    public void logStatus() throws java.rmi.RemoteException {
        galgelogikShared.logStatus();
    }


    public void hentOrdFraDr() throws Exception {
        galgelogikShared.hentOrdFraDr();
    }

    public String getStatusBesked() throws java.rmi.RemoteException {
        return galgelogikShared.getStatusBesked();
    }
}
