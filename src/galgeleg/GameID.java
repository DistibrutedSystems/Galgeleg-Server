package galgeleg;

import brugerautorisation.LoginValidator;
import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.RMI.IGameID;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class GameID extends UnicastRemoteObject implements IGameID {

    public GameID() throws Exception {
        HashMap<String, IGalgelogikShared> gameList = new HashMap<>();
    }


    @Override
    public IGalgelogikRMI get(String id) throws RemoteException {
        return (IGalgelogikRMI) GalgelegInformationExpert.getInstance().getSpilListe().get(id);
    }

    @Override
    public void put(String id, IGalgelogikRMI game, Map<String, String> login) throws RemoteException {
        if(LoginValidator.validate(login)){
            GalgelegInformationExpert.getInstance().getSpilListe().put(id, game);
        }
    }

    @Override
    public void delete(String id, Map<String, String> login) throws RemoteException {

    }
}
