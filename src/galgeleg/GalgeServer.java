/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgeleg;

import brugerautorisation.RMI.BrugerAutorisationImplRMI;
import brugerautorisation.SOAP.BrugerAutorisationImplSOAP;

import java.io.IOException;
import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * @author Tobias
 */
public class GalgeServer {

    public static void main(String[] arg) throws IOException, Exception {

        System.out.println("Publicerer IBrugeradmin over RMI");
        BrugerAutorisationImplRMI impl = new BrugerAutorisationImplRMI();
        LocateRegistry.createRegistry(2002); // start rmiregistry i server-JVM
        Naming.rebind("rmi://localhost:2002/galgelogik", impl);
        System.out.println("IBrugeradmin publiceret over RMI");

        System.out.println("Publicerer IBrugeradmin over SOAP");
        BrugerAutorisationImplSOAP impl2 = new BrugerAutorisationImplSOAP();
        // Ipv6-addressen [::] svarer til Ipv4-adressen 0.0.0.0, der matcher alle maskinens netkort og
        //Endpoint.publish("http://0.0.0.0:9940/galgelogik", impl2);
        //System.out.println("IBrugeradmin publiceret over SOAP");

        Game game = new Game();
        //BrugerAutorisationImplRMI aut = new BrugerAutorisationImplRMI();
        GameID gameID = new GameID();
        Highscore highscore = new Highscore();
//        System.out.println("Laver galg    elogik som RMI");
//        //System.setProperty("java.RMI.server.hostname", "ubuntu4.saluton.dk"); // set hostname
//        // TODO: 07-Mar-18 Ã†ndre registry til vores server
//        LocateRegistry.createRegistry(2008); // start rmiregistry i server-JVM
//        Naming.rebind("rmi://localhost/galgelogik", aut);
        Naming.rebind("rmi://localhost:2002/Game", game);
        Naming.rebind("rmi://localhost:2002/GameID", gameID);
        Naming.rebind("rmi://localhost:2002/Highscore", highscore);
//
/*
        System.out.println("GalgelogikImplRMI tilgÃ¦ngelig over RMI");
        Naming.lookup("rmi://localhost:2002/Game");

        System.out.println("GalgelogikImplRMI tilgÃ¦ngelig over RMI");
*/

    }

}
