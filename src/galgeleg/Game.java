package galgeleg;

import brugerautorisation.LoginValidator;
import galgeleg.Implementation.GalgelogikShared;
import galgeleg.Implementation.RMI.GalgelogikImplRMI;
import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.RMI.IGame;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

public class Game extends UnicastRemoteObject implements IGame {


    public Game() throws java.rmi.RemoteException, Exception {

    }

    public String put(Map<String, String> login) throws Exception {
        if(LoginValidator.validate(login)){
            GalgelegInformationExpert.getInstance().getSpilListe().put(login.get("username"), new GalgelogikImplRMI());
            return login.get("username");
        }
        return null;
    }

    public HashMap<String, IGalgelogikRMI> get() throws Exception {
        return GalgelegInformationExpert.getInstance().getSpilListe();
    }
}
