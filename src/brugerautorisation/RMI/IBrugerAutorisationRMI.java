/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brugerautorisation.RMI;

import galgeleg.interfaces.RMI.IGalgelogikRMI;

/**
 *
 * @author Tobias Højsgaard
 */
public interface IBrugerAutorisationRMI extends java.rmi.Remote{
    IGalgelogikRMI login(String brugernavn, String password) throws Exception;
    
}
