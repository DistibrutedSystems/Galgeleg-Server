/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brugerautorisation.RMI;

import brugerautorisation.LoginValidator;
import brugerautorisation.transport.rmi.Brugeradmin;
import galgeleg.GalgelegInformationExpert;
import galgeleg.GameID;
import galgeleg.Implementation.GalgelogikShared;
import galgeleg.Implementation.RMI.GalgelogikImplRMI;
import galgeleg.interfaces.RMI.IGalgelogikRMI;
import galgeleg.interfaces.RMI.IGameID;
import galgeleg.interfaces.interfaces.IGalgelogikShared;
import java.rmi.Naming;

import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Tobias Højsgaard
 */
public class BrugerAutorisationImplRMI extends UnicastRemoteObject implements IBrugerAutorisationRMI {

    private IGameID gameID = new GameID();
    public BrugerAutorisationImplRMI() throws java.rmi.RemoteException, Exception {
        
    }
    
    @Override 
    public IGalgelogikRMI login(String username, String password) throws Exception{
        //Brugeradmin ba = (Brugeradmin) Naming.lookup("rmi://javabog.dk/brugeradmin");
        
        Map<String, String> login = new HashMap<>();
        login.put("username", username);
        login.put("password", password);
        
        if(LoginValidator.validate(login)){

            IGalgelogikRMI game = gameID.get(username);
            if(game != null){
                return game;
            } else {
                GalgelogikImplRMI newGame = new GalgelogikImplRMI();
                HashMap<String, IGalgelogikRMI> spil = GalgelegInformationExpert.getInstance().getSpilListe();
                spil.put(username, newGame);
                GalgelegInformationExpert.getInstance().setSpilListe(spil);
                logGame(username);
                return newGame;
            }
        }
        return null;
    }

    public void logGame(String username){

        System.out.println(username + " started a game");
    }


}
