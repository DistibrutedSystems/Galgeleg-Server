/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brugerautorisation.SOAP;

import galgeleg.Implementation.SOAP.GalgelogikImplSOAP;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author Tobias Højsgaard
 */
@WebService
public interface IBrugerAutorisationSOAP{
    @WebMethod
    GalgelogikImplSOAP login(String brugernavn, String password) throws Exception;
    
}
