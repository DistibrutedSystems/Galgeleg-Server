package brugerautorisation.SOAP;

import brugerautorisation.IBrugeradmin;
import brugerautorisation.data.Bruger;
import galgeleg.GalgelegInformationExpert;
import galgeleg.GameID;
import galgeleg.Implementation.GalgelogikShared;
import galgeleg.Implementation.SOAP.GalgelogikImplSOAP;
import galgeleg.interfaces.RMI.IGameID;
import galgeleg.interfaces.interfaces.IGalgelogikShared;

import javax.jws.WebService;
import java.rmi.Naming;

@WebService(endpointInterface = "brugerautorisation.SOAP.BrugerAutorisationImplSOAP")
public class BrugerAutorisationImplSOAP implements IBrugerAutorisationSOAP {

    private IGameID gameID = new GameID();

    public BrugerAutorisationImplSOAP() throws Exception {
    }

    public GalgelogikImplSOAP login(String username, String password) throws Exception{
        /*
        IBrugeradmin ba = (IBrugeradmin) Naming.lookup("rmi://javabog.dk/brugeradmin");

        Bruger b = ba.hentBruger(username, password); //Hvis brugeren ikke er gyldig får de en exception

        IGalgelogikShared game = (GalgelogikShared) gameID.get(username);
        if(game != null){
            return new GalgelogikImplSOAP(game);
        } else {
            GalgelogikImplSOAP newGame = new GalgelogikImplSOAP();
            GalgelegInformationExpert.getInstance().getSpilListe().put(username, newGame.galgelogikShared);
            logGame(username);
            return newGame;
        }*/
        return new GalgelogikImplSOAP();
    }

    public void logGame(String username){

        System.out.println(username + " started a game");
    }


}
