/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brugerautorisation;

import brugerautorisation.transport.rmi.Brugeradmin;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tobias
 */
public class LoginValidator {
    public static boolean validate(Map<String, String> login){
        String user = login.get("username");
        String pass = login.get("password");
        
        if(user != null && pass != null){
            try {
                Brugeradmin ba = (Brugeradmin) Naming.lookup("rmi://javabog.dk/brugeradmin");
                if(ba.hentBruger(user, pass) != null){
                    return true;
                }
            } catch (NotBoundException ex) {
                Logger.getLogger(LoginValidator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (MalformedURLException ex) {
                Logger.getLogger(LoginValidator.class.getName()).log(Level.SEVERE, null, ex);
            } catch (RemoteException ex) {
                Logger.getLogger(LoginValidator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }
    
}
